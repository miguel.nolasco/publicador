package mx.com.banamex.afore.publicador.service;

import com.sun.org.apache.xpath.internal.operations.Mult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;

@Service
public class FileService {
    private static final Logger LOG = LoggerFactory.getLogger(FileService.class);
    private String destinationDir;
    private static final String SLASH = "/";

    public FileService(@Value("${destination.path}") String destinationDir) {
        this.destinationDir = destinationDir;
    }

    public void transfer(List<MultipartFile> multipartFiles) throws IOException {
        String time = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String newDir = SLASH + destinationDir + SLASH + time;
        LOG.info("Creando directorio {}", newDir);
        File dir = new File(newDir);
        dir.mkdir();
        LOG.info("OK");
        multipartFiles.stream().parallel().forEach(m -> {
            Path filepath = Paths.get(newDir, m.getOriginalFilename());
            try {
                m.transferTo(filepath);
            } catch (IOException e) {
                LOG.error(e.toString());
            }
        });
//        for(MultipartFile m : multipartFiles){
//            Path filepath = Paths.get(newDir, m.getOriginalFilename());
//            try {
//                m.transferTo(filepath);
//            } catch (Exception e) {
//                LOG.error(e.toString());
//                throw e;
//            }
//        }
    }
}
