package mx.com.banamex.afore.publicador.controller;

import mx.com.banamex.afore.publicador.service.FileService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/file")
@CrossOrigin
public class PublicadorController {
    private FileService fileService;

    public PublicadorController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping(value = "/upload", produces = "application/json", consumes = "multipart/form-data")
    @ResponseStatus(code = HttpStatus.OK)
    public void uploadFiles(@RequestPart("file") List<MultipartFile> multipartFiles) throws IOException {
        fileService.transfer(multipartFiles);
    }
}
